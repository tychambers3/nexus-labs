module ApplicationHelper
  def devise_error_messages(resource)
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
      <div>
        #{messages}
      </div>
    HTML

    html.html_safe
  end

  def active_route?(path)
    "active" if current_page?(path)
  end
end
