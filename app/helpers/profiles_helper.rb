module ProfilesHelper
  def full_name
    @profile = Profile.find(params[:user_id])
    @profile.first_name.capitalize + " " + @profile.last_name.capitalize
  end

  def current_user_full_name
    @profile = Profile.find(current_user.id)
    @profile.first_name.capitalize + " " + @profile.last_name.capitalize
  end
end
