class UsersController < ApplicationController

  def index
  end

  def show
    
  end

  def update
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    if @user.destroy
      flash[:notice] = "User deleted. We hope to see you again!"
      redirect_to root_path
    end
  end
end
