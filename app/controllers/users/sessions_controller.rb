class Users::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #
  # end

  # POST /resource/sign_in
  # def create
  #   redirect_to user_profiles_path(current_user)
  # end

  # DELETE /resource/sign_out
  # def destroy
  #
  # end

  def after_sign_in_path_for(resource)
    user_profiles_path(resource)
  end
  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_in_params
   devise_parameter_sanitizer.permit(:sign_in, keys: [:email])
  end
end
