class EpisodesController < ApplicationController
  before_action :find_podcast, except: [:destroy]

  def new
    @episode = @podcast.episodes.new
  end

  def create
    @episode = @podcast.episodes.new(episode_params)

    if @episode.save
      flash[:notice] = "Episode created!"
      redirect_to podcast_path(@podcast)
    else
      flash[:alert] = "Something went wrong"
      render :new
    end
  end

  def edit
    @episode = Episode.find(params[:id])
  end

  def update
    @episode = Episode.find(params[:id])

    if @episode.update(episode_params)
      flash[:notice] = "Episode updated"
      redirect_to podcast_path(@podcast)
    else
      flash[:alert] = "Something went wrong"
      render :edit
    end
  end

  def destroy
    @episode = Episode.find(params[:podcast_id])
    @podcast = Podcast.find(params[:id])

    if @episode.destroy
      flash[:notice] = 'Episode deleted'
      redirect_to podcast_path(@podcast.id)
    end
end
  protected

  def find_podcast
		@podcast = Podcast.find(params[:podcast_id])
	end
  def episode_params
    params.require(:episode).permit(:title, :description, :episode_thumbnail, :upload_file, :keywords, :audio)
  end

end
