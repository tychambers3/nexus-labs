class PodcastsController < ApplicationController
  before_action :authenticate_user!, except: [:show]

  def index

  end
  def new
    @podcast = Podcast.new
  end

  def create
    @podcast = Podcast.new(podcast_params)
    @podcast.user_id = current_user.id

    if @podcast.save
      flash[:notice] = "Podcast Created!"
      redirect_to podcast_path(@podcast)
    else
      flash[:alert] = "Something went wrong. Try again."
      render :new
    end
  end

  def show
    @podcast = Podcast.find(params[:id])
    @episode = @podcast.episodes.all
    @q = @episode.ransack(params[:q])
    @episode = @q.result

    @search_path = ""
  end

  def edit
    @podcast = Podcast.find(params[:id])
  end

  def update
    @podcast = Podcast.find(params[:id])
    if @podcast.update(podcast_params)
      flash[:notice] = 'Podcast updated'
      redirect_to podcast_path
    else
      flash[:alert] = "Something went wrong"
      render :new
    end
  end

  def destroy
    @podcast = Podcast.find(params[:id])

    if @podcast.destroy
      flash[:notice] = "Your podcast has been deleted"
      redirect_to user_profiles_path(current_user)
    end
  end
  protected

  def podcast_params
    params.require(:podcast).permit(:title, :podcast_thumbnail, :description, :owner, :manager, :keywords)
  end
end
