class ProfilesController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :check_for_profile, only: [:show]

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id

    if @profile.save
      flash[:notice] = 'Your profile has been created'
      redirect_to user_profiles_path(current_user)
    else
      flash[:alert] = "Something went wrong"
      render :new
    end
  end

  def show
    @user = User.find(params[:user_id])
    @profile = Profile.all
    @podcast = @user.podcasts.order(:created_at)
  end

  def edit
    @profile = Profile.find(params[:user_id])
  end

  def update
    @profile = Profile.find(current_user.id)

    if @profile.update(profile_params)
      flash[:notice] = 'Updated'
      redirect_to user_profiles_path(current_user)
    else
      puts @profile.errors.full_messages
      render :edit
    end
  end

  protected

  def check_for_profile
    if !current_user.profile
      flash[:alert] = "You need to create a profile"
      redirect_to new_user_profiles_path(current_user)
    end
  end

  def profile_params
    params.require(:profile).permit(:username, :first_name, :last_name, :portfolio_site, :twitter, :dribbble, :behance, :avatar)
  end
end
