class ExploresController < ApplicationController
  def index
    @q = Podcast.ransack(params[:q])
    @podcast = @q.result

    @search_path = 'explore'
  end
end
