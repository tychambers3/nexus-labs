class Episode < ApplicationRecord
  belongs_to :podcast

  has_attached_file :episode_thumbnail, styles: { x_large: '1920x940>', thumb: '100x100#' }, :url => '/podcast/episode/thumb/:id/:style/:basename.:extension', :path => ':rails_root/public/podcast/episode/thumb/:id/:style/:basename.:extension'

  has_attached_file :audio, :url => '/podcast/episode/audio/:id/:basename.:extension', :path => ':rails_root/public/podcast/episode/audio/:id/:basename.:extension'

  validates_attachment_content_type :audio, :content_type => ['audio/mpeg', 'audio/mp3', 'audio/mpeg-4', 'audio/x-mpeg4', 'audio/mpeg3', 'audio/m4a', 'audio/mp4', 'audio/aac']

  validates_attachment_content_type :episode_thumbnail, :content_type => ['image/jpg', 'image/jpeg', 'image/png']


  validates(:title, :presence => true, :length => { :maximum => 50 })
  validates(:description, :presence => true)

  validates(:keywords, :presence => true)

  validates_with AttachmentSizeValidator, attributes: :episode_thumbnail, less_than: 1.megabytes
end
