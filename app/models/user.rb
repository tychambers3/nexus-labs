class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  has_one :profile, dependent: :destroy
  has_many :podcasts, dependent: :destroy

  accepts_nested_attributes_for :profile

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  email_regex = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates(:email, :presence => true, :format => { :with => email_regex }, :uniqueness => { :case_sensitive => false })

  validates(:password, :presence => true, :confirmation => true, :length => { :within => 8..41 })


end
