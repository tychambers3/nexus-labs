class Profile < ApplicationRecord
  belongs_to :user

  has_attached_file :avatar, styles: { large: '500x500>', medium: '300x300>', thumb: '100x100#' }, :default_url => ':rails_root/app/assets/images/default_thumb.jpg', :url => '/profiles/avatars/:id/:style/:basename.:extension', :path => ':rails_root/public/profiles/avatars/:id/:style/:basename.:extension'

  validates(:username, :presence => true, :length => { :maximum => 50 })
  validates(:first_name, :presence => true, :length => { :maximum => 50 })
  validates(:last_name, :presence => true, :length => { :maximum => 50 })
  validates(:portfolio_site, :presence => false, :length => { :maximum => 255 })
  validates(:twitter, :presence => false, :length => { :maximum => 255 })
  validates(:dribbble, :presence => false, :length => { :maximum => 255 })
  validates(:behance, :presence => false, :length => { :maximum => 255 })

  validates_attachment_content_type :avatar, :content_type => ['image/jpg', 'image/jpeg', 'image/png']

  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 1.megabytes

end
