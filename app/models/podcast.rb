

class Podcast < ApplicationRecord


  belongs_to :user
  has_many :episodes, dependent: :destroy

  accepts_nested_attributes_for :episodes

  has_attached_file :podcast_thumbnail, styles: { x_large: '1920x940>', medium: '250x250#', thumb: '150x150#' }, :default_url => ':rails_root/public/podcast/defaults/:style/default.jpg', :url => '/podcast/thumb/:id/:style/:basename.:extension', :path => ':rails_root/public/podcast/thumb/:id/:style/:basename.:extension'

  validates_attachment_content_type :podcast_thumbnail, :content_type => ['image/jpg', 'image/jpeg', 'image/png']

  validates(:title, :presence => true, :length => { :maximum => 50 })
  validates(:description, :presence => true)

  validates_with AttachmentSizeValidator, attributes: :podcast_thumbnail, less_than: 3.megabytes


end
