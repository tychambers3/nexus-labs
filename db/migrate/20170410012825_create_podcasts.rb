class CreatePodcasts < ActiveRecord::Migration[5.0]
  def change
    create_table :podcasts do |t|
      t.string :title
      t.text :description
      t.string :thumbnail
      t.string :cover_photo
      t.string :manager
      t.string :owner

      t.timestamps
    end
  end
end
