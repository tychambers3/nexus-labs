class AddKeywordsToPodcasts < ActiveRecord::Migration[5.0]
  def change
    add_column :podcasts, :keywords, :string
  end
end
