class AddAttachmentPodcastThumbnailToPodcasts < ActiveRecord::Migration
  def self.up
    change_table :podcasts do |t|
      t.attachment :podcast_thumbnail
    end
  end

  def self.down
    remove_attachment :podcasts, :podcast_thumbnail
  end
end
