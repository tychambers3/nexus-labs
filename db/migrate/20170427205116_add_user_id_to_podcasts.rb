class AddUserIdToPodcasts < ActiveRecord::Migration[5.0]
  def change
    add_reference :podcasts, :user, foreign_key: true
  end
end
