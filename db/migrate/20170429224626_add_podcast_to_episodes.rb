class AddPodcastToEpisodes < ActiveRecord::Migration[5.0]
  def change
    add_reference :episodes, :podcast, foreign_key: true
  end
end
