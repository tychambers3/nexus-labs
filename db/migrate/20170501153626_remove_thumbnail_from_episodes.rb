class RemoveThumbnailFromEpisodes < ActiveRecord::Migration[5.0]
  def change
    remove_column :episodes, :thumbnail, :string
  end
end
