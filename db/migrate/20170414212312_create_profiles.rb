class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :profile_picture
      t.string :portfolio_site
      t.string :twitter
      t.string :dribbble
      t.string :behance

      t.timestamps
    end
  end
end
