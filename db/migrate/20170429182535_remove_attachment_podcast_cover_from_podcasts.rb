class RemoveAttachmentPodcastCoverFromPodcasts < ActiveRecord::Migration[5.0]
  def change
    remove_attachment :podcasts, :podcast_cover
  end
end
