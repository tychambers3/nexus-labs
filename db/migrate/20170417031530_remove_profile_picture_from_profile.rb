class RemoveProfilePictureFromProfile < ActiveRecord::Migration[5.0]
  def change
    remove_column :profiles, :profile_picture, :string
  end
end
