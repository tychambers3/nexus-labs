class RemoveThumbnailAndCoverPhotoFromPodcasts < ActiveRecord::Migration[5.0]
  def change
    remove_column :podcasts, :thumbnail, :string
    remove_column :podcasts, :cover_photo, :string
  end
end
