class CreateEpisodes < ActiveRecord::Migration[5.0]
  def change
    create_table :episodes do |t|
      t.string :title
      t.text :description
      t.string :thumbnail
      t.string :upload_file
      t.string :keywords

      t.timestamps
    end
  end
end
