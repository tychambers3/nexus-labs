class AddAttachmentPodcastCoverToPodcasts < ActiveRecord::Migration
  def self.up
    change_table :podcasts do |t|
      t.attachment :podcast_cover
    end
  end

  def self.down
    remove_attachment :podcasts, :podcast_cover
  end
end
