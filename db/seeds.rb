# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# 5.times do |i|
#   Podcast.create(title: 'Grow Your Code #{i}',  description: 'Next level listicle lomo occupy celiac, drinking vinegar fap wolf. Marfa listicle tilde vaporware echo park. Truffaut pickled cliche direct trade pabst vexillologist, street art chicharrones dreamcatcher sriracha. Live-edge tumeric single-origin coffee, banh mi polaroid pork belly pug 90\'s 8-bit lumbersexual. Mixtape fam messenger bag, street art kickstarter live-edge whatever thundercats art party twee church-key iPhone. Kombucha meh banh mi bushwick, cred portland sustainable kinfolk bespoke ramps pour-over cray narwhal. Mixtape kombucha lomo offal, selfies poke unicorn fixie cardigan coloring book.',  manager: 'A. Wookie, Hugh Jackman',  owner: 'Tyrel Chambers')
#   puts 'Podcast: ##{i} Created'
# end
