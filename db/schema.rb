# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170501153719) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "episodes", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "keywords"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "episode_thumbnail_file_name"
    t.string   "episode_thumbnail_content_type"
    t.integer  "episode_thumbnail_file_size"
    t.datetime "episode_thumbnail_updated_at"
    t.integer  "podcast_id"
    t.string   "audio_file_name"
    t.string   "audio_content_type"
    t.integer  "audio_file_size"
    t.datetime "audio_updated_at"
    t.index ["podcast_id"], name: "index_episodes_on_podcast_id", using: :btree
  end

  create_table "podcasts", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "manager"
    t.string   "owner"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id"
    t.string   "podcast_thumbnail_file_name"
    t.string   "podcast_thumbnail_content_type"
    t.integer  "podcast_thumbnail_file_size"
    t.datetime "podcast_thumbnail_updated_at"
    t.string   "keywords"
    t.index ["user_id"], name: "index_podcasts_on_user_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "portfolio_site"
    t.string   "twitter"
    t.string   "dribbble"
    t.string   "behance"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "username"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "episodes", "podcasts"
  add_foreign_key "podcasts", "users"
  add_foreign_key "profiles", "users"
end
