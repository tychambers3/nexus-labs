# README

## ToDo
- Create parallax cover photo for podcast show page
- Add error messages to all forms and stuff
- Remove cover-photo from podcast

## Studio
- Invite users by email to studio. Each user can be given a permission level. Can't invite more than your pricing plan allows.
- Live waveform of recording.
- Edit waveform in browser
- Audio controls
- Search box for host users podcasts
- Assign that episode to that podcast
- Record audio of room
- Edit Episode details in the studio
- Allow audio and picture upload as well
