require 'rails_helper'

describe 'sign up page', :type => :feature do

  it 'can be reached' do
    visit '/signup'
    expect(page.status_code).to eq(200)
  end

  it 'signs me up' do
    visit '/signup'
    within(".info-form") do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: 'password'
      fill_in 'user_password_confirmation', with: 'password'
    end

    click_button 'Sign up'
    expect(page).to have_content 'Welcome to Nexus Labs!'
  end


end
