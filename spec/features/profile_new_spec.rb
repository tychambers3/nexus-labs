require 'rails_helper'

describe 'new profile page', :type => :feature do
  before(:each) do
    visit "/users/1/profiles/new"
  end
  it 'can be reached' do
    expect(page.status_code).to eq(200)
  end

  it 'can be created' do
    within(".sign-up-form") do
      fill_in 'profile_username', with: 'chambers43'
    end

    click_button 'Create'

  end
end
