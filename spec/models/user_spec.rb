require 'rails_helper'
require 'spec_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user = {
      :email => 'user@user.com',
      :password => 'password',
      :password_confirmation => 'password'
    }
  end

  it 'should create a user if valid' do
    User.create!(@user)
  end

  describe 'email' do
    it 'should accept valid email addresses' do
      addresses = %w[user@foo.com THE_user@foo.bar.org user.user@foo.jp]
      addresses.each do |a|
        valid_email_user = User.new(@user.merge(:email => a))
        expect(valid_email_user).to be_valid
      end
    end

    it 'should reject invalid email addresses' do
      addresses = %w[user@foo,com user_at_foo.org user.user@foo.]
      addresses.each do |a|
        invalid_email_user = User.new(@user.merge(:email => a))
        expect(invalid_email_user).not_to be_valid
      end
    end

    it 'should reject duplicate emails' do
      User.create!(@user)
      user_with_duplicate_email = User.new(@user)
      expect(user_with_duplicate_email).not_to be_valid
    end

    it 'should reject email addresses identical up to case' do
      upcased_email = @user[:email].upcase
      User.create!(@user.merge(:email => upcased_email))
      user_with_duplicate_email = User.new(@user)
      expect(user_with_duplicate_email).not_to be_valid
    end
  end

  describe 'password validations' do
    it 'should require a password' do
      expect(User.new(@user.merge(:password => '', :password_confirmation => ''))).not_to be_valid
    end

    it 'should require a matching password confirmation' do
      expect(User.new(@user.merge(:password_confirmation => 'invalid'))).not_to be_valid
    end

    it 'should reject short passwords' do
      short = 'a' * 5
      hash = @user.merge(:password => short, :password_confirmation => short)
      expect(User.new(hash)).not_to be_valid
    end

    it 'should reject long passwords' do
      long = 'a' * 42
      hash = @user.merge(:password => long, :password_confirmation => long)
      expect(User.new(hash)).not_to be_valid
    end
  end

  describe 'password encryption' do
    before(:each) do
      @user = User.create!(@user.merge(:password => 'foobar11', :password_confirmation => 'foobar11'))
    end

    it 'should have an encrypted password attribute' do
      expect(@user).to respond_to(:encrypted_password)
    end
  end
end
