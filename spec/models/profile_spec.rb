require 'rails_helper'

RSpec.describe Profile, type: :model do
  before(:each) do
    @profile = {
      :username => 'Chambers43',
      :first_name => 'John',
      :last_name => 'Doe',
      :portfolio_site => 'my-site.com',
      :twitter => 'user22',
      :dribbble => 'user22',
      :behance => 'user22'
    }
  end

  it 'can be created if valid' do
    Profile.create!(@profile)
  end

  describe 'username' do
    it 'cannot be created without a username' do
      no_username = Profile.new(@profile.merge(:username => ''))
      expect(no_username).to_not be_valid
    end

    it 'should reject long usernames' do
      long_username = 'a' * 51
      long_username_profile = Profile.new(@profile.merge(:username => long_username))
      expect(long_username_profile).not_to be_valid
    end
  end

  describe 'names' do
    it 'cannot be created without a first_name' do
      no_first_name = Profile.new(@profile.merge(:first_name => ''))
      expect(no_first_name).not_to be_valid
    end

    it 'cannot be created without a last_name' do
      no_last_name = Profile.new(@profile.merge(:last_name => ''))
      expect(no_last_name).not_to be_valid
    end

    it 'first_name cannot be over 50 characters' do
      long_name_first = 'a' * 51
      long_name_first_profile = Profile.new(@profile.merge(:first_name => long_name_first))
      expect(long_name_first_profile).not_to be_valid
    end

    it 'last_name cannot be over 50 characters' do
      long_name_last = 'a' * 51
      long_name_last_profile = Profile.new(@profile.merge(:last_name => long_name_last))
      expect(long_name_last_profile).not_to be_valid
    end
  end

  describe 'social media links' do
    it 'should reject long portfolio_site links' do
      long_portfolio = 'a' * 256
      long_portfolio_link = Profile.new(@profile.merge(:portfolio_site => long_portfolio))
      expect(long_portfolio_link).not_to be_valid
    end

    it 'should reject long twitter links' do
      long_twitter = 'a' * 256
      long_twitter_link = Profile.new(@profile.merge(:twitter => long_twitter))
      expect(long_twitter_link).to_not be_valid
    end

    it 'should reject long dribbble links' do
      long_dribbble = 'a' * 256
      long_dribbble_link = Profile.new(@profile.merge(:dribbble => long_dribbble))
      expect(long_dribbble_link).to_not be_valid
    end

    it 'should reject long behance links' do
      long_behance = 'a' * 256 
      long_behance_link = Profile.new(@profile.merge(:behance => long_behance))
      expect(long_behance_link).to_not be_valid
    end
  end

end
