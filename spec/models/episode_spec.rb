require 'rails_helper'
require 'spec_helper'

RSpec.describe Episode, type: :model do
  before(:each) do
    @episode = {
      :title => "How to podcast",
      :description => 'Tutorial Podcasting',
      :thumbnail => 'https://unsplash.com/?photo=BiyVuXGiF7s',
      :upload_file => 'https://drive.google.com/open?id=0B7EIi9rsmomuVk1oNk1GOEVaaFk',
      :keywords => 'Development, Design, Life'
    }
  end

  it 'should create a new episode if valid' do
    Episode.create!(@episode)
  end

  describe 'title' do
    it 'should require a title' do
      no_title_episode = Episode.new(@episode.merge(:title => ''))
      expect(no_title_episode).to_not be_valid
    end

    it ' should reject names too long' do
      long_name = 'a' * 51
      long_name_title = Episode.new(@episode.merge(:title => long_name))
      expect(long_name_title).not_to be_valid
    end
  end

  describe 'description' do
    it 'should require a description' do
      no_description = Episode.new(@episode.merge(:description => ''))
      expect(no_description).not_to be_valid
    end
  end

  describe 'file upload' do
    it 'should require a file to upload' do
      no_file_upload = Episode.new(@episode.merge(:upload_file => ''))
      expect(no_file_upload).not_to be_valid
    end
  end

  describe 'keywords' do
    it 'should require atleast 1 keyword' do
      one_keyword_episode = Episode.new(@episode.merge(:keywords => ''))
      expect(one_keyword_episode).not_to be_valid
    end
  end

end
