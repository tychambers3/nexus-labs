require 'rails_helper'
require 'spec_helper'

RSpec.describe Podcast, type: :model do
  before(:each) do
    @podcast = {
      :title => 'Grow Your Code',
      :description => 'I help people get by in this world',
      :thumbnail => 'https://unsplash.com/?photo=BiyVuXGiF7s'
    }
  end

  it 'should create a new podcast if valid' do
    Podcast.create!(@podcast)
  end

  describe 'title' do
    it 'should require a title' do
      no_title_podcast = Podcast.new(@podcast.merge(:title => ''))
      expect(no_title_podcast).to_not be_valid
    end

    it ' should reject names too long' do
      long_name = 'a' * 51
      long_name_title = Podcast.new(@podcast.merge(:title => long_name))
      expect(long_name_title).not_to be_valid
    end
  end

  describe 'description' do
    it 'should require a description' do
      no_description = Podcast.new(@podcast.merge(:description => ''))
      expect(no_description).not_to be_valid
    end
  end
end
