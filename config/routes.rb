Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    get 'login', to: 'users/sessions#new'
    get 'signup', to: 'users/registrations#new'
    get 'logout', to: 'users/sessions#destroy'
  end

  resources :users do
    resource :profiles
  end

  resources :podcasts do
    resource :episodes
  end

  get 'users/:user_id/studio' => 'studios#show', as: :studio

  get 'explore' => 'explores#index'
  get 'explore/search' => 'explores#index'

  delete 'podcasts/:podcast_id/episodes/:id' => 'episodes#destroy',  :as => :destroy_episode

  get 'podcasts/:podcast_id/episodes/:id/edit' => 'episodes#edit', :as => :update_episode

  patch 'podcasts/:podcast_id/episodes/:id/edit' => 'episodes#update'

  get '/users/:id' => 'users/registations#destroy', :via => :delete, :as => :destroy_user

  get 'users/:user_id/new' => 'profiles#new'
  root 'welcome#index'
end
